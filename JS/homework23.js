"use strict";

//Упражнение 1 

let value = prompt("Введите любое число","");
let intervalId = setInterval(() => {
    if (value) {
        value = value - 1;
        console.log(`Осталось:${value}`);
            if (value === 0) {
            clearInterval(intervalId);
            console.log(`Время вышло!`);
            }
    }
    else {
        alert('нет такого значения!');
        clearInterval(intervalId);
    };
},1000);


//Упражнение 2

let promise = fetch("https://reqres.in/api/users");
promise
    .then(function(response){
        return response.json();
    })
    .then(function(response) {
        let users = response.data;
        console.log(`Получили пользователей ${users.length}`);
        users.forEach(function(user){
        console.log(`- ${user.first_name} ${user.last_name
        } (${user.email})`);
})
});


