"use strict";

//Упражнение 1 

let numbers = [5,6,7,8,9,10];
console.log(numbers);

let summ = 0;
function getSumm(numbers){
           for (let number of numbers) {
            summ=summ+number;
            };
            return summ; 
};

console.log(getSumm(numbers));

let newNumbers = numbers.map(function(n){
    return n*2
});

console.log(newNumbers);

newNumbers.shift();
newNumbers.push("Frontenders");
newNumbers.push("!");
newNumbers.unshift("Hello");
console.log(newNumbers);

let arr = [3,{},5,6,7,8,9,"a",10];
console.log(arr);

let summ2 = 0;
function getSumm2(arr) {
    for (let i=0; i<arr.length; i++) {
    if (typeof arr[i] =='number') {
        summ2=summ2+arr[i];     
    } else {
        continue; 
        };
    };
return summ2;
};
console.log(getSumm2(arr));

//Упражнение 2

let cart = [4884];
console.log(cart);

function addToCart(productId) {
    let hasInCart = cart.includes(productId);
    if (hasInCart) return;
    cart.push(productId);
};

addToCart(3456);
console.log(cart);

function removeFromCart(productId){
    cart =  cart.filter(function(id){
        return id !== productId
    });
};

removeFromCart(4884);
console.log(cart);