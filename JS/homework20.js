"use strict";

//Упражнение 1
for (let i = 0; i <= 20; i = i + 1){
if (i %2 != 0) continue;
console.log(i);
}

//Упражнение 2
let sum = 0;
for (let i = 0; i < 3; i = i + 1){
let value = +prompt("Введите число","");
if(value){
sum+= value;
}
else{
alert("Ошибка, вы ввели не число!");
};
}
alert("Сумма: "+sum);


//Упражнение 3
function getNameOfMonth(month) {
    if (month === 0) return 'Январь' 
    if (month === 1) return 'Февраль' 
    if (month === 2) return 'Март' 
    if (month === 3) return 'Апрель' 
    if (month === 4) return 'Май' 
    if (month === 5) return 'Июнь' 
    if (month === 6) return 'Июль' 
    if (month === 7) return 'Август' 
    if (month === 8) return 'Сентябрь' 
    if (month === 9) return 'Октябрь' 
    if (month === 10) return 'Ноябрь' 
    if (month === 11) return 'Декабрь' 
    }
    let result = getNameOfMonth(0)
    console.log(result);
    for (let month = 0; month < 12; month = month+1) {
    const m = getNameOfMonth(month);
    if(m === 'Октябрь') continue;
    console.log(m);
    }

//Упражнение 4
// IIFE - «immediately-invoked function expressions». 
//Здесь создаётся и немедленно вызывается 
//Function Expression. Так что код выполняется сразу
// же и у него есть свои локальные переменные.
//IIFE выглядит так:
(function() {
let message = "Hello";
console.log(message); 
}())
