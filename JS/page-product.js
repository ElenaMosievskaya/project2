document.addEventListener("DOMContentLoaded", function(event) {
    console.log("DOM fully loaded and parsed");
  });

let form = document.querySelector('.form');

let inputNameContainer = form.querySelector('.first-row');
let inputScoreContainer = form.querySelector('.first-row');

let inputName = inputNameContainer.querySelector('.input1');
let errorNameElem = form.querySelector('.name-error');

let inputScore = inputScoreContainer.querySelector('.input2');
let errorNameScore = form.querySelector('.score-error');

let inputTextarea = form.querySelector('.textarea');

console.log(inputName, inputScore, inputTextarea);


function handleSubmit(event) {
    event.preventDefault();

        let name = inputName.value;
        let score = +inputScore.value;

    
        //for name
        let errorName = "";
            if (name.length === 0) {
                errorName = "Вы забыли указать имя и фамилию!";
            } else if (name.length < 2) {
                errorName = "Имя не может быть короче 2-х символов!";
            } 
            if (errorName) {
                errorNameElem.innerText = errorName;
            }
            if (errorName) {
                errorNameElem.classList.add('visible');
            } else {
                errorNameElem.classList.remove('visible');
            }
            if (errorName) return;

        //for score
        let errorScore = "";
            if (score > 5 || score <1) {
                errorScore = "Оценка должна быть от 1 до 5!";
            } 
            if (isNaN(score)) {
                errorScore = "Оценка должна быть от 1 до 5!";
            } 
            if (errorScore) {
                errorNameScore.innerText = errorScore;
            }
            if (errorScore) {
                errorNameScore.classList.add('visible');
            } else {
                errorNameScore.classList.remove('visible');
                localStorage.removeItem('formData');
            } 

}


let formData = {};

function handleInput(event){
    formData[event.target.name] = event.target.value;
    localStorage.setItem("formData", JSON.stringify(formData));
}

if (localStorage.getItem('formData')) {
    formData = JSON.parse(localStorage.getItem('formData'));
        for (let key in formData) {
            form.elements[key].value = formData[key];
            }
}

document.addEventListener('submit', handleSubmit);
form.addEventListener("input", handleInput);

// Аттестация

let roundCountBusket = document.querySelector('.bigbusket-round');

let goodsCountBusket = document.querySelector('.bigbusket-count');

let btnAddToBusket = document.querySelector('.busket');


console.log(goodsCountBusket);

goodsCountBusket.innerHTML = localStorage.getItem('basketOne');

btnAddToBusket.addEventListener("click", function() { 
    if (!localStorage.getItem('basketOne')){
        goodsCountBusket.innerHTML = "1";
        localStorage.setItem("basketOne", goodsCountBusket.innerHTML); 
        roundCountBusket.style.display = "block"; 
        btnAddToBusket.innerHTML = "Товар добавлен в корзину";
    }
    else {
        localStorage.removeItem('basketOne');
        goodsCountBusket.innerHTML = ""; 
        roundCountBusket.style.display = "none";
        btnAddToBusket.innerHTML = "Добавить в корзину";
    }});

    if (localStorage.getItem('basketOne')) {     
    roundCountBusket.style.display = "block"; 
    btnAddToBusket.innerHTML = "Товар добавлен в корзину";
    
    } else {}


