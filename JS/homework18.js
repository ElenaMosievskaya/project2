"use strict";

//Упражнение 1
let a='100px';
let b='323px';
let result = parseInt(a) + parseInt(b);
console.log(result);

//Упражнение 2

console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

//Упражнение 3
let d=0.111;
console.log(Math.ceil(d));

let e=45.333333;
console.log(e.toFixed(1));

let f=3;
console.log(3**5);

let g=400000000000000;
console.log(4e14);

let h='1'!=1;
console.log(Boolean(h='1'==1));

//Упражнение 4
console.log(0.1 + 0.2 === 0.3);
//Число хранится в памяти в бинарной форме, 
//как последовательность бит – единиц и нулей. 
//Но дроби, такие как 0.1, 0.2, которые выглядят довольно
// просто в десятичной системе счисления, 
//на самом деле являются бесконечной дробью в двоичной форме.